create database if not exists bookstore;

use bookstore;

create table inserts
(
    insert_id    int primary key auto_increment,
    insert_count int,
    insert_date  date
) engine = INNODB;

create table sales
(
    sales_id    int primary key auto_increment,
    sales_count int,
    sales_date  date
) engine = INNODB;


create table book
(
    book_id     int primary key auto_increment,
    name        varchar(255),
    author_name varchar(255),
    book_price  double,
    FOREIGN KEY book (book_id)
        REFERENCES inserts (insert_id),
    FOREIGN KEY book (book_id)
        REFERENCES sales (sales_id)
) engine = INNODB;


create table client
(
    client_id            int primary key auto_increment,
    client_name          varchar(255),
    client_phone         varchar(255),
    client_post_index    varchar(255),
    client_index_address varchar(255),
    client_index_phone   varchar(255),
    FOREIGN KEY client_id (client_id)
        REFERENCES book (book_id)
) engine = INNODB;

